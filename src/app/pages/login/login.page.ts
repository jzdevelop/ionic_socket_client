import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {WebsocketService} from '../../services/websocket.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user:string;
  password:string;
  urlServer:string
  constructor(private router: Router) { }

  ngOnInit() {
  }

  validateUser(){
    console.log(typeof(this.user));
    if(this.user === "juez1@settle.com" && this.password === "bnf123*" ){
      window.localStorage.setItem('judge',this.user);
      let url = "http://" + this.urlServer;
      window.localStorage.setItem('urlServer',url);
      this.router.navigate(['/judge-score']);
    }
    else if(this.user === "juez2@settle.com" && this.password === "bnf123*" ){
      window.localStorage.setItem('judge',this.user);
      let url = "http://" + this.urlServer;
      window.localStorage.setItem('urlServer',url);
      this.router.navigate(['/judge-score']);
    }
    else if(this.user === "juez3@settle.com" && this.password === "bnf123*" ){
      window.localStorage.setItem('judge',this.user);
      let url = "http://" + this.urlServer;
      window.localStorage.setItem('urlServer',`${url}`);
      this.router.navigate(['/judge-score']);
    }
  }

}
