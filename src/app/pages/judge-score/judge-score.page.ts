import { Component, OnInit } from '@angular/core';
import {WebsocketService} from '../../services/websocket.service';
import {SendDataService} from '../../services/send-data.service';
@Component({
  selector: 'app-judge-score',
  templateUrl: './judge-score.page.html',
  styleUrls: ['./judge-score.page.scss'],
})
export class JudgeScorePage implements OnInit {

  score:number[] = [0, 0, 0, 0];
  itemsJudges:string[] = [];
  category:string;
  competitor:string;
  itemsJudge1: any = {
    judge: "judge1",
    item1: "Composición artística, creatividad, originalidad",
    item2: "Efectos visuales, formación y transición",
    item3: "Completación misical",
    item4: "Nivel de dificultad"
  }
  itemsJudge2: any = {
    judge: "judge2",
    item1: "Control, fuerza y conexión",
    item2: "Ejecución adecuada de los movimientos técnicos de danza",
    item3: "Sincronización y uniformidad",
    item4: "Proyección y presencia escénica"
  }
  itemsJudge3: any = {
    judge: "judge3",
    item1: "Desplazamiento en el escenario",
    item2: "Vestuario",
    item3: "Entretenimiento",
    item4: "Conexión con el público"
  }

  constructor(private websocketService: WebsocketService, private sendDataService: SendDataService) { }

  ngOnInit() {
    this.seleectJudge();
    this.websocketService.listen("chat:message").subscribe((data:any)=>{
      this.category = data.username;
      this.competitor = data.message;
    });
  }

  roundScore(dato:number){
    return Math.round(dato * 10) / 10;
  }
  seleectJudge(){
    let judge:string = window.localStorage.getItem('judge');
    if(judge === "juez1@settle.com"){
      this.itemsJudges.push(this.itemsJudge1.item1);
      this.itemsJudges.push(this.itemsJudge1.item2);
      this.itemsJudges.push(this.itemsJudge1.item3);
      this.itemsJudges.push(this.itemsJudge1.item4);
    }
    else if(judge === "juez2@settle.com"){
      this.itemsJudges.push(this.itemsJudge2.item1);
      this.itemsJudges.push(this.itemsJudge2.item2);
      this.itemsJudges.push(this.itemsJudge2.item3);
      this.itemsJudges.push(this.itemsJudge2.item4);
    }
    else if(judge === "juez3@settle.com"){
      this.itemsJudges.push(this.itemsJudge3.item1);
      this.itemsJudges.push(this.itemsJudge3.item2);
      this.itemsJudges.push(this.itemsJudge3.item3);
      this.itemsJudges.push(this.itemsJudge3.item4);
    }
  }

  sendData(){
    let judge:string = window.localStorage.getItem('judge').substr(0,5);
    let data = {
      judge: judge,
      competitor_name: this.competitor,
      category: this.category,
      item1: this.score[0],
      item2: this.score[1],
      item3: this.score[2],
      item4: this.score[3],
    }
    console.log(data);
    this.sendDataService.postData(data).subscribe((res)=>{
      console.log(res);
    });
    
  }

}
