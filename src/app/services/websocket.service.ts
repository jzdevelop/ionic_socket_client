import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  socket:any;
  constructor() {
    let url:string = window.localStorage.getItem('urlServer'); 
    this.socket = io(`${url}`);
  }

  listen(eventName: string){
    return new Observable((subscriber)=>{
      this.socket.on(eventName, (data) =>{
        subscriber.next(data);
      });
    });
  }
}
