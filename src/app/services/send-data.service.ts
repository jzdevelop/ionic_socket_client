import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SendDataService {

  constructor(private http: HttpClient) { }

  postData(data:any): Observable<any>{
    let urlServer:string = window.localStorage.getItem('urlServer');
    return this.http.post(`${urlServer}/add-ratings`,data);
  }
}
